import {
  CREATE_MESSAGE,
  CREATE_MESSAGE_SUCCESS,
  CREATE_MESSAGE_FAILURE,
  LIST_MESSAGE,
  LIST_MESSAGE_SUCCESS,
  LIST_MESSAGE_FAILURE,
} from "../actions";
const INITIAL_STATE = {
  messages: [],
  message: {},
  loading: false,
  error: "",
};

export const messageReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_MESSAGE:
      return {
        ...state,
        loading: true,
      };
    case CREATE_MESSAGE_SUCCESS:
      const { message } = action.payload;
      return {
        ...state,
        loading: false,
        error: "",
        message: message,
      };
    case CREATE_MESSAGE_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    case LIST_MESSAGE:
      return {
        ...state,
        loading: true,
      };
    case LIST_MESSAGE_SUCCESS:
      const { messages } = action.payload;
      return {
        ...state,
        loading: false,
        error: "",
        messages: messages,
      };
    case LIST_MESSAGE_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};
