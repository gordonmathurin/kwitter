import { LIST_USERS, LIST_USERS_SUCCESS, LIST_USERS_FAILURE } from "../actions/listOfUsers"

const INITIAL_STATE = {
  users: [],
  loading: false,
  error: ""
}

export const listOfUsersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LIST_USERS:
      return {
        ...state,
        loading: true
      };
    case LIST_USERS_SUCCESS:
      const { users } = action.payload
      return {
        ...state,
        loading: false,
        error: "",
        users: users
      };
    case LIST_USERS_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    default:
      return state;
  }
}

// <<<<<<<<<<<<below is what the server will return>>>>>>>>>>>>
// {
//   "users": [
//     {
//       "username": "string",
//       "displayName": "string",
//       "about": "string",
//       "createdAt": "2020-06-11T20:10:39.471Z",
//       "updatedAt": "2020-06-11T20:10:39.472Z",
//       "pictureLocation": "string",
//       "googleId": "string"
//     }
//   ],
//   "count": 0,
//   "statusCode": 0
// }