import {
  SET_PIC,
  SET_PIC_SUCCESS,
  SET_PIC_FAILURE,
} from "../actions/setPicture";
// import ShadowImg from "../../images/ShadowImg";

const INITIAL_STATE = {
  username:"",
  picture:"",
  loading: false,
  error: "",
};

export const setPictureReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_PIC:
      return {
        ...INITIAL_STATE,
        loading: true,
      };

    case SET_PIC_SUCCESS:
      return {
        ...INITIAL_STATE,
        newUser: action.payload.user,
        loading: false,
      };

    case SET_PIC_FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};
