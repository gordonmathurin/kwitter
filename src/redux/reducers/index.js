import { combineReducers } from "redux";
import { authReducer } from "./auth";
import { createUserReducer, deleteUserReducer } from "./createUser"
import { updateUserReducer } from "./updateUser"
import { listOfUsersReducer } from "./listOfUsers"
import { postLikesReducer } from "./likes"
import { messageReducer } from "./messages"

export default combineReducers({
  auth: authReducer,
  create: createUserReducer,
  userupdate: updateUserReducer,
  delete: deleteUserReducer,
  list: listOfUsersReducer,
  messages: messageReducer,
  like: postLikesReducer

});
