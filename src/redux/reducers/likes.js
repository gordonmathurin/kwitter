import { LIKE, LIKE_SUCCESS, LIKE_FAILURE } from "../actions/likes"


const INITIAL_STATE = {
    likesCount: {},
    loading: false,
    error: ""
}

export const postLikesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIKE:
            return {
                ...INITIAL_STATE,
                loading: true
            }

        case LIKE_SUCCESS:
            return {
                ...INITIAL_STATE,
                likesCount: action.payload,
                loading: false
            }

        case LIKE_FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false
            }

        default:
            return state
    }

}
