import { UPDATE_USERNAME, UPDATE_SUCCESS, UPDATE_FAILURE, } from '../actions/updateUser'

const INITIAL_STATE = {
  userUpdate: {

  },
  isAuthenticated: false,
  user: "",
  loading: false,
  error: "",
}

export const updateUserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_USERNAME:
      return {
        ...INITIAL_STATE,
        loading: true
      }

    case UPDATE_SUCCESS:
      const { user, token } = action.payload;
      return {
        ...INITIAL_STATE,
        isAuthenticated: token,
        user,
        loading: false
      }

    case UPDATE_FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false
      }
    default:
      return state
  }
}