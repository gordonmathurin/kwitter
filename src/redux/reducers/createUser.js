import { CREATE, CREATE_SUCCESS, CREATE_FAILURE, DELETE_SUCCES, DELETE_FAILURE, DELETE } from '../actions/createUser'


// subject to change
const INITIAL_STATE = {
    userNew: {

    },
    loading: false,
    error: ""
};

export const createUserReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CREATE:
            return {
                ...INITIAL_STATE,
                loading: true
            }

        case CREATE_SUCCESS:
            return {
                ...INITIAL_STATE,
                userNew: action.payload.user,
                loading: false
            }

        case CREATE_FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false
            }

        default:
            return state
    }
}


export const deleteUserReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case DELETE:
            return {
                ...INITIAL_STATE,
                loading: true
            }

        case DELETE_SUCCES:
            const { username } = action.payload;
            return {
                ...INITIAL_STATE,
                username,
                loading: false
            }

        case DELETE_FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false
            }

        default:
            return state

    }
}
