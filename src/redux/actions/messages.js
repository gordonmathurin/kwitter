import api from "../../utils/api";

export const CREATE_MESSAGE = "CREATE_MESSAGE";
export const CREATE_MESSAGE_SUCCESS = "CREATE_MESSAGE_SUCCESS";
export const CREATE_MESSAGE_FAILURE = "CREATE_MESSAGE_FAILURE";
export const GET_MESSAGE = "GET_MESSAGE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const LIST_MESSAGE = "LIST_MESSAGE";
export const LIST_MESSAGE_SUCCESS = "LIST_MESSAGE_SUCCESS";
export const LIST_MESSAGE_FAILURE = "LIST_MESSAGE_FAILURE";

export const create_message = (msg) => async (dispatch, getState) => {
  try {
    dispatch({ type: CREATE_MESSAGE });
    const payload = await api.createMessage(msg);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    dispatch({
      type: CREATE_MESSAGE_SUCCESS,
      payload: payload,
    });
    dispatch(list_message())
    // console.log({ payload });
  } catch (err) {
    dispatch({
      type: CREATE_MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};
export const list_message = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LIST_MESSAGE });
    const payload = await api.listMessage();
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    dispatch({
      type: LIST_MESSAGE_SUCCESS,
      payload: payload,
    });
    // console.log({ payload });
  } catch (err) {
    dispatch({
      type: LIST_MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};
