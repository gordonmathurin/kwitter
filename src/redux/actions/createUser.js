//Testing has been completed. 6/9/2020 @10:34 EST
import api from "../../utils/api";
import { logout } from "./auth";

export const CREATE = "USER/CREATE";
export const CREATE_SUCCESS = "USER/CREATE_SUCCESS";
export const CREATE_FAILURE = "USER/CREATE_FAILURE";
export const DELETE = "USER/DELETE";
export const DELETE_SUCCES = "USER/DELETE_SUCCES";
export const DELETE_FAILURE = "USER/DELETE_FAILURE";

const redirect = () => {
  return window.location = "http://localhost:3000/"
}

export const createUser = (username, displayName, password) => async (
  dispatch,
  getState
) => {
  try {
    console.log(username, displayName, password);
    dispatch({ type: CREATE });
    const payload = await api.createUser(username, displayName, password);

    dispatch({
      type: CREATE_SUCCESS,
      payload: payload,
    });
    dispatch(setTimeout(redirect(), 10000))
  } catch (err) {
    dispatch({
      type: CREATE_FAILURE,
      payload: err.message,
    });
  }
};

export const deleteUser = (username) => async (dispatch, getState) => {
  try {
    dispatch({ type: DELETE });
    const payload = await api.deleteUser(username)

    dispatch({
      type: DELETE_SUCCES,
      payload: payload,
    })
    dispatch(logout())
  } catch (err) {
    dispatch({
      type: DELETE_FAILURE,
      payload: err.message,
    });
  }
};
