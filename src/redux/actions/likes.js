
import api from "../../utils/api";
import { list_message } from "./messages";


export const LIKE = "LIKE_START"
export const LIKE_SUCCESS = "LIKE_SUCCESS"
export const LIKE_FAILURE = "LIKE_FAILURE"

export const postLike = (messageId) => async (dispatch, getState) => {
    try {
        console.log(messageId)
        dispatch({ type: LIKE })
        const payload = await api.getpostlikes(messageId)
        dispatch({
            type: LIKE_SUCCESS,
            payload: payload
        })
        dispatch(list_message())
    } catch (err) {
        dispatch({
            type: LIKE_FAILURE,
            payload: err.message
        })
    }

}
