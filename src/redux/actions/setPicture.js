import api from "../../utils/api";

export const SET_PIC = "USER/SET_PIC";
export const SET_PIC_SUCCESS = "USER/SET_PIC_SUCCESS";
export const SET_PIC_FAILURE = "USER/SET_PIC_FAILURE";

export const setPicture = (picture) => async (dispatch, getState) => {
  const {
    auth: { username },
  } = getState();
  // console.log('in setPicture', username, picture);

  
  
  // picture.append("username", "picture")
  
  try {
    dispatch({ type: SET_PIC, picture});
     await api.setPicture(username, picture);
    
    dispatch({
      type: SET_PIC_SUCCESS,
     
    });
  } catch (err) {
    dispatch({
      type: SET_PIC_FAILURE,
      payload: err.message,
    });
  }
};
