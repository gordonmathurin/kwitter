import api from "../../utils/api"

//all my exports
export const LIST_USERS = "LIST_USERS";
export const LIST_USERS_SUCCESS = "LIST_USERS_SUCCESS";
export const LIST_USERS_FAILURE = "LIST_USERS_FAILURE";

export const list_users = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LIST_USERS }); //this ships to listOfUserReducer() action to let UI know its loading
    const payload = await api.listUsers(); //need to create the api called listUsers()
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({
      type: LIST_USERS_SUCCESS,
      payload: payload,
    });
    console.log({ payload });
  } catch (err) {
    dispatch({
      type: LIST_USERS_FAILURE,
      payload: err.message,
    });
  }
};

