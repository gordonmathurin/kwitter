import React from "react";
import { LoginFormContainer, MenuContainer } from "../components";

export const HomeScreen = () => (
         <>
           <MenuContainer />
           <h2>First... Let us know who you are</h2>
           <LoginFormContainer />
           <footer>
             <div className="login-footer">
               Proudly presented by Kenzie Academy Jan 2020 SE Students:
               <div className="names">
                 Gordon Mathurin, Erica Best, Kevin Clark, and Greg Hodges
               </div>
             </div>
           </footer>
         </>
       );
