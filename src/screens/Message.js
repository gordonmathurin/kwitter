import React from "react";
import {MenuContainer, MessagesContainer, ListMessageContainer } from "../components";

export const MessageScreen = () => (
         <>
           <MenuContainer />
           <h2>Messages...</h2>
           <div className="messages-container">
             <MessagesContainer />
           </div>
           <div className="list-messages-container">
             <ListMessageContainer />
           </div>
         </>
       );
