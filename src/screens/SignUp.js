import React from "react";

import { CreateUserForm } from "../createNewUser/CreateUserForm";
/////////////////////WE NEED TO EXPORT A CreatUserEnhancer.js to pass in to this screen
export const SignUpScreen = () => (
  <React.Fragment>
    <div className="Sign-up-Box">{CreateUserForm}</div>
  </React.Fragment>
);
