import React from "react";
import { MenuContainer } from "../components";
import SetUserPicture from "../components/setUserPicture/SetUserPicture.js";
import UpdateUser from "../components/updateUser/UpdateUser";
import { DeleteUserContainer } from "../components/deleteUser";




export const ProfileScreen = () => (
  <>
    <MenuContainer />
    <h2>Your Profile</h2>
    <p></p>
    <SetUserPicture />
    <UpdateUser />
    <DeleteUserContainer />

    {/* <ListOfUsersContainer /> */}
    <footer>
      <div className="footer">
        Proudly presented by Kenzie Academy Jan 2020 SE Students:
               <div className="names">
          <b>
            Gordon Mathurin, Erica Best, Kevin Clark, and Greg Hodges
                 </b>
        </div>
      </div>
    </footer>
  </>
);



