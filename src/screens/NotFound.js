import React from "react";
import { Link } from "react-router-dom";
import { MenuContainer } from "../components";
import fourOfour from "../images/fourOfour.png"

const NotFound = ({ location }) => (
  <React.Fragment>
    <MenuContainer />
    <div className="not-found-container">
      <div className="not-found-container">
        <div className="not-found-info">
          <p>Page not found for {location.pathname}</p>
        </div>

        <Link to="/" className="go-home-button">
          Back to profile
        </Link>
      </div>
      <img className="fourOfour" src={fourOfour} alt="Failed to load" />
      <h2>Did you watch my demo bruh??</h2>
    </div>
  </React.Fragment>
);

export const NotFoundScreen = NotFound
