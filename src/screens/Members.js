import React from "react";
import { MenuContainer } from "../components";
import { ListOfUsersContainer } from "../components/list-of-users";
// import { MessageScreen } from "./Message";


const Members = () => (
  <>
    <MenuContainer />
    <h2>Other Kwitter's</h2>
      <ListOfUsersContainer />
    <footer>
      <div className="footer">
        Proudly presented by Kenzie Academy Jan 2020 SE Students:
        <div className="names">
          Gordon Mathurin, Erica Best, Kevin Clark, and Greg Hodges
        </div>
      </div>
    </footer>
  </>
);
export const MembersScreen = Members;

