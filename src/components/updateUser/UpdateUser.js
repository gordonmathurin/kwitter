import React, { useState, } from 'react'
import ProptTypes from "prop-types"
import { connect } from 'react-redux'
import { Loader } from "../loader"
import { updateUserName } from '../../redux/actions/auth'


const UpdateUser = ({ username, displayName, updateUserName, loading, error }) => {
  const [user, setUser] = useState({
    displayName: ""
  })

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setUser((prevState) => ({ ...prevState, [inputName]: inputValue }))
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    // console.log(user.displayName)
    console.log(user.displayName)
    updateUserName(user.displayName)
    setUser({ displayName: "" })
  }

  return (
    <div className="user-container">
      {/* <div>User Name: {username} </div>
      <div>Display Name: {displayName}</div> */}

      <form onSubmit={handleSubmit} className="user-container">
        {/* <h3 className="green-text text-darken-3">Update Display Name</h3> */}

        <div className="change-display-name-input">
          <input
            type="text"
            name="displayName"
            value={user.displayName}
            onChange={handleChange}
            placeholder="I prefer to be called..."
            required
            autoFocus
          />
        </div>


        <button className="change-user-button" type="submit">
          Update Display Name
          </button>
        {loading && <Loader />}
        {error && <p style={{ color: "red" }}>{error.message}</p>}

      </form>
    </div>
  );
}

UpdateUser.propTypes = { //validating a prop
  updateUserName: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};

const mapStateToProps = (state) => ({
  loading: state.userupdate.loading,
  error: state.userupdate.error,
  username: state.auth.username,
  displayName: state.auth.displayName,
})

const mapDispatchToProp = {
  updateUserName, //this is my action located in 
}

export default connect(mapStateToProps, mapDispatchToProp)(UpdateUser)
// this connect() attaches this page and props to the store







