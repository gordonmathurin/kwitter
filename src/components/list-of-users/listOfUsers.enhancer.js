import { connect } from "react-redux"
import { list_users } from "../../redux/actions/listOfUsers"

const mapStateToProps = (state) => ({
    loading: state.list.loading,
    error: state.list.error,
    listUsers: state.list.users
})

const mapDispatchToProps = {
    list_users,
}

export const enhancer = connect(mapStateToProps, mapDispatchToProps)