import React, { useEffect, useState } from 'react'
import ProptTypes from "prop-types"
import { Loader } from "../loader"



export const ListOfUsers = ({ list_users, loading, error, listUsers }) => {

  const [search, setSearch] = useState("")
  const [filteredUsers, setFilteredUsers] = useState([])


  useEffect(() => {
    list_users();
  }, [list_users]);

  useEffect(() => {
    setFilteredUsers(
      listUsers.filter(users => {
        return users.displayName.toLowerCase().includes(search.toLocaleLowerCase())
      })
    )
    console.log(filteredUsers)
  }, [search, listUsers])

  return (
    <React.Fragment>
      {/* {listUsers.map((users) => {
          return (
            <div key={users.username}>
              <p>{`@${users.displayName}`}</p>
            </div>
          );
        })} */}

      <div className="search-users">
        <input
          className="search-box"
          type="text"
          placeholder="Search for people on Kwitter"
          autoFocus
          onChange={(event) => setSearch(event.target.value)}
        />
      </div>
      <h6 className="current-search-user">You are currently Searching for:</h6>{" "}
      <h5>{search}</h5>
      <div>
        {filteredUsers.map((users) => {
          return (
            <div className="card-small" key={users.createdAt}>
              <div className="profile-card-image">
                <img
                  className="card-user-photo"
                  src={`https://kwitter-api.herokuapp.com/users/${users.username}/picture`}
                  alt=""
                />
              </div>
              <div className="card-content">
                <span className="card-title activator red-text text-darken-4">
                  <a href={`https://kwitter-api.herokuapp.com/users/${users.username}/picture`}>
                    {`@${users.displayName}`}
                  </a>
                  {/* <i className="material-icons right">more_vert</i> */}
                </span>
                <p className="cursive-text"><b>User Info </b><br />
                  {users.about}
                </p>
                <div className="about"></div>
              </div>
              <div className="card-reveal">
                {/* <span className="card-title red-text text-darken-4">
                Card Title<i className="material-icons right"></i>
              </span> */}
                <p className="message-id"><strong>{`${new Date(users.createdAt)}`}</strong></p>
              </div>
            </div>
          );
        })}
      </div>
      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};

ListOfUsers.propTypes = {
  list_users: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
  listUsers: ProptTypes.array,
};
