import { enhancer } from './listOfUsers.enhancer'
import { ListOfUsers } from "./ListOfUsers"

export const ListOfUsersContainer = enhancer(ListOfUsers)