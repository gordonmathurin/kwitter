import { connect } from "react-redux";
import { create_message, list_message } from "../../redux/actions/messages";
import { postLike } from "../../redux/actions/likes"

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) => ({
  message: state.messages.message,
  loading: state.messages.loading,
  error: state.messages.error,
  listMessage: state.messages.messages,
});

// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
  create_message,
  list_message,
  postLike,

};

export const enhancer = connect(mapStateToProps, mapDispatchToProps);
