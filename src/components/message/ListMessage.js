import React, { useEffect } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";


export const ListMessage = ({ username, list_message, loading, error, listMessage, postLike }) => {

  const handleLike = (messageId) => event => {
    event.preventDefault();
    postLike(messageId)
  };

  useEffect(() => {
    list_message();
  }, [list_message]);


  return (
    <React.Fragment>
      {listMessage.map((messages) => {
        return (
          <div key={messages.id} className="col s10 m7">
            <div className="card-message-small">
              <div className="card-message-photobox">
                <img
                  className="message-image"
                  src={`https://kwitter-api.herokuapp.com/users/${messages.username}/picture`}
                  alt=""
                />
              </div>
              <div className="card-stacked">
                <div className="card-content">
                  <p id="messageInfo">
                    <b>{messages.username.toUpperCase()}:</b>{" "}
                    <h4 className="cursive-text">{` ${messages.text} `}</h4>
                    <i className="message-id">{`at ${new Date(
                      messages.createdAt
                    )}`}</i>
                  </p>
                </div>
                <div className="card-action">
                  <button
                    className="likeButton"
                    onClick={handleLike(messages.id)}
                  >
                    <p>
                      <i className="material-icons">mood</i>{" "}
                      {messages.likes.length}
                    </p>
                  </button>
                </div>
              </div>
            </div>
          </div>
        );
      })}

      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment >
  )
}

//Added this to see if We could get the users pictured loaded in the message list
// const mapStateToProps = (state) => ({
//   username: state.auth.username,
// })
///////////////////////////////////////////////////////////////
ListMessage.propTypes = {
  postLike: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
  listMessage: ProptTypes.array,

};


