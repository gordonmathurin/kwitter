import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";

export const CreateMessage = ({ create_message, loading, error }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    text: "",
  });

  const handleText = (event) => {
    event.preventDefault();
    create_message(state);
    setState({ text: "" })
  };

  const handleChange = (event) => {
    const inputText = event.target.value;
    const text = event.target.name;

    setState((prevState) => ({ ...prevState, [text]: inputText }));
  };

  //event.target.text = ""

  return (
    <React.Fragment>
      <form className="message-input" onSubmit={handleText}>
        
        <input
        className="message-input"
          type="text"
          name="text"
          placeholder="Say something..."
          value={state.text}
          autoFocus
          required
          onChange={handleChange}
        />

        <button type="submit" onClick={handleText} disabled={loading}>
          Send
        </button>
      </form>
      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};

CreateMessage.propTypes = {
  create_message: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};
