import { enhancer } from "./message.enhancer";
import { CreateMessage } from "./createmessage";
import { ListMessage } from "./ListMessage";
export const MessagesContainer = enhancer(CreateMessage);
export const ListMessageContainer = enhancer(ListMessage);
