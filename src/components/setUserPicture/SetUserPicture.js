import React, { useState } from "react";
import ProptTypes from "prop-types";
import { connect } from "react-redux";
import { setPicture } from "../../redux/actions/setPicture";
import "./SetUserPicture.css";
const SetUserPicture = ({
  setPicture,
  username,
  user,
  displayName,
  picture,
  loading,
  error,
}) => {
  const [photo, setPhoto] = useState(null);

  const onImageChange = (event) => {
    const file = event.target.files[0];
    console.log(file);
    setPhoto(file);
  }; // debugger;

  const handleSubmit = (event) => {
    event.preventDefault();

    setPicture(photo);

    console.log(photo);
  };

  return (
    <div className="container">
      <div className="user-name">
        <b>Username</b>
        <i>
          <b>{username}</b>
        </i>
      </div>
      <div className="image-container">
        <img
          src={`https://kwitter-api.herokuapp.com/users/${username}/picture`}
          alt="Nothing Uploaded"
        />
      </div>
      <form className="container" onSubmit={handleSubmit}>
        <div className="display-name">
          <b>Display Name </b>
          <div>
            <i>{displayName}</i>
          </div>
        </div>
        <input
          className="choose-photo-box"
          onChange={onImageChange}
          name="picture"
          type="file"
          required
          autoFocus
        />
        <button className="upload-button" type="submit">
          Upload
        </button>
        <p className=" photo-instructions">
          Click on the Profile link in the menu to view your uploaded photo{" "}
        </p>
      </form>
    </div>
  );
};

SetUserPicture.propTypes = {
  setPicture: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};

const mapStateToProps = (state) => ({
  loading: state.create.loading,
  username: state.auth.username,
  displayName: state.auth.displayName,
  error: state.create.error,
});

const mapDispatchToProp = {
  setPicture,
};

export default connect(mapStateToProps, mapDispatchToProp)(SetUserPicture);
