import React from "react";
import ProptTypes from "prop-types";
import { Link } from "react-router-dom";
import "./Menu.css";
import { ProfileScreen } from "../../screens";
import { MembersScreen } from "../../screens/Members";
import { MessageScreen } from "../../screens";


export const Menu = ({ isAuthenticated, logout }) => {
  return (
    <div className="total-page">
      <div id="menu">
        <h1>Kwitter</h1>

        {isAuthenticated && (
          <div id="menu-links">
            <Link to="/" onClick={ProfileScreen}>
              Profile
            </Link>
            <Link to="/messagefeed" onClick={MessageScreen}>
              Message Feed
              </Link>
            <Link to="/members" onClick={MembersScreen}>
              Members
            </Link>
            <Link to="/" onClick={logout}>
              Logout
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

Menu.defaultProps = {
  isAuthenticated: false,
  logout: () => { },
};

Menu.propTypes = {
  isAuthenticated: ProptTypes.bool.isRequired,
  logout: ProptTypes.func.isRequired,
};
