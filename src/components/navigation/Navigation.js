import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import {
  HomeScreen,
  ProfileScreen,
  NotFoundScreen,
  MessageScreen,
} from "../../screens";
import { MembersScreen } from "../../screens/Members"
import { ConnectedRoute } from "../connected-route/ConnectedRoute";
// import SignUpToLogin from "../connected-route/ConnectedToLogin"
import CreateUserForm from '../createNewUser/CreateUserForm'

export const Navigation = () => (
         <BrowserRouter>
           <Switch>
             <ConnectedRoute
               exact
               path="/"
               redirectIfAuthenticated
               component={HomeScreen}
             />
             <ConnectedRoute
               exact
               isProtected
               path="/profiles/:username"
               component={ProfileScreen}
             />

             <ConnectedRoute
               exact
               path="/signup"
               redirectIfAuthenticated
               component={CreateUserForm}
             />

             <ConnectedRoute
               exact
               isProtected
               path="/messagefeed"
               component={MessageScreen}
             />

             <ConnectedRoute
               exact
               isProtected
               path="/members"
               component={MembersScreen}
             />

             <ConnectedRoute path="*" component={NotFoundScreen} />
           </Switch>
         </BrowserRouter>
       );
