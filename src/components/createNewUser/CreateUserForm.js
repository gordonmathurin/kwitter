import React, { useState } from "react";
import ProptTypes from "prop-types";
import { connect } from "react-redux";
import { Loader } from "../loader";
import { createUser } from "../../redux/actions/createUser";


const CreateUserForm = ({ createUser, loading, error }) => {
  const [state, setState] = useState({
    username: "",
    displayName: "",
    password: "",
  });

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };


  const handleSubmit = (event) => {
    event.preventDefault();
    createUser(state);
    setState({
      username: "",
      displayName: "",
      password: ""
    })
  };

  return (
    <div className="total-page">
      <div id="menu">
        <h1>Kwitter</h1>
      </div>

      <div className="sign-up-container">
        <form onSubmit={handleSubmit} className="">
          <h5 className="">Sign Up</h5>
          <div className="inputfield">
            <input
              type="text"
              name="username"
              value={state.username}
              placeholder="UserName"
              onChange={handleChange}
              required
              autoFocus
            />
          </div>

          <div className="input-field">
            <input
              type="text"
              name="displayName"
              value={state.displayName}
              placeholder="DisplayName"
              onChange={handleChange}
              //required
              autoFocus
            />
          </div>

          <div className="input-field">
            <input
              type="password"
              name="password"
              value={state.password}
              placeholder="Password"
              onChange={handleChange}
              required
              autoFocus
            />
          </div>
          <div className="input-field">
            <button className="btn green lighten-1 z-depth-0" type="submit">
              Submit
            </button>
            {/* have to fix up loader Component later  */}
            {loading && <Loader />}
            {error && <p style={{ color: "red" }}>{error.message}</p>}
          </div>

          {/* WILL HAVE TO HOOK UP TO ROUTE CORRECTLY */}

          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0" type="submit">
              Cancel
            </button>
            {loading && <Loader />}
            {error && <p style={{ color: "red" }}>{error.message}</p>}
          </div>
        </form>
        <p>
          Once you have <b>Successfully</b> created an account, <b>Submit</b>{" "}
          will redirect you back to the login page.
        </p>
      </div>
    </div>
  );
};


CreateUserForm.propTypes = {
  createUser: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};

const mapStateToProps = (state) => ({
  loading: state.create.loading,
  error: state.create.error,
});

const mapDispatchToProp = {
  createUser,
};

export default connect(mapStateToProps, mapDispatchToProp)(CreateUserForm);
