import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import "./LoginForm.css";
import { Link } from "react-router-dom";

export const LoginForm = ({ login, loading, error }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    username: "",
    password: "",
  });
  //console.log(state)

  const handleLogin = (event) => {
    event.preventDefault();
    login(state);
  }

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <React.Fragment>
      <div className="Sign-Up-Button-Box">
        <Link to="/signup">
          <button className="Create-User-Account">Creat a user Account</button>
        </Link>
      </div>
      <form id="login-form" onSubmit={handleLogin}>
        <div className="logBox">
          <label htmlFor="username"></label>
          <input
            className="userName"
            type="text"
            name="username"
            placeholder="UserName"
            value={state.username}
            autoFocus
            required
            onChange={handleChange}
          />
          <label htmlFor="password"></label>
          <input
            className="passWord"
            type="password"
            name="password"
            placeholder="Password"
            value={state.password}
            required
            onChange={handleChange}
          />
          {/* button is disabled and not working */}

          <button type="submit">Login</button>
        </div>
      </form>

      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};

LoginForm.propTypes = {
  login: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};
