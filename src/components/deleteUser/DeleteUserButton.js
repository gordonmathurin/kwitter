import React from 'react'
import ProptTypes from "prop-types"
import { Loader } from "../loader";



export class DeleteUser extends React.Component {

    deleteHandle = (event) => {
        event.preventDefault()
        this.props.deleteUser(this.props.user)
    }

    render() {
        return (
            <div className="delete-container">
                <button className="delete-user-button" loading={<Loader />}
                    onClick={this.deleteHandle} > Delete User </button>
                    <p>Caution! This cannot be undone. 
                    Once you delete a user, all data will be perminantly lost</p>
            </div>


        )
    }

}

DeleteUser.propTypes = {
    deleteUser: ProptTypes.func.isRequired,
    loading: ProptTypes.bool,
    error: ProptTypes.string,
};

