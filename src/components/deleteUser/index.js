import { enhancer } from "./DeleteUser.enhancer";
import { DeleteUser } from "./DeleteUserButton";

export const DeleteUserContainer = enhancer(DeleteUser);
