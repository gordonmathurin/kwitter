import { connect } from "react-redux";
import { deleteUser } from "../../redux/actions/createUser"

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) => ({
    user: state.auth.username,
    loading: state.delete.loading,
    error: state.delete.error
});

// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
    deleteUser,
};

export const enhancer = connect(mapStateToProps, mapDispatchToProps);
